# Bigger is Greater

This is my solution to the Bigger is Greater problem on hackerrank.com

This isn't the predominant next_permutation solution(https://www.nayuki.io/page/next-lexicographical-permutation-algorithm), but a custom solution I came up with.  My solution uses a BST map and a hash map to track the order and frequency of the characters seen, so that the re-ordering of the end of the string can be done with O(n) complexity.  Thus, the total worst case asymptotic complexity is O(n), assuming an efficiently implemented hash map.

The classic solution, while effectively equal in asymptotic complexity, is likely more efficient than this one in many practical cases.  However, I found this use of data structures to be elegant enough to be worth sharing.

## Authors

* **Keith Jordan** - [contact](mailto:3keithjordan3@gmail.com)

## License

This project is licensed under the GNU General Public License - see the [COPYING](COPYING) file for details


