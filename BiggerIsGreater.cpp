/**
*
* My solution to the Bigger is Greater challenge on hackerrank.com
*
* Copyright (C) 2018 by Keith Jordan <3keithjordan3@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
* 
**/

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>

using namespace std;

string getBigger(string s) {

	// tracks the frequency of each letter
	unordered_map<char, int> charFrequency;
	// bst of char to index for most recently seen occurrence of that char
	// since there are only 26 possible chars, this contributes maximum log(26) complexity
	// to the algorithm, which is asymptotically constant
	map<char, int> charTree;

	// flag to indicate whether its possible to create a greater string
	bool isPossible = false;
	// iterate in reverse through the string
	for (int i = s.length() - 1; i >= 0; --i) {
		// record this as most recent occurrance of this char
		charTree[s[i]] = i;
		if (charFrequency.find(s[i]) == charFrequency.end())
			charFrequency[s[i]] = 0;
		charFrequency[s[i]]++;
		auto treeIter = charTree.find(s[i]);
		// if there is a char greater than this one, put the least greater char in this
		// spot, and sort the rest of the string from i to the end
		if (++treeIter != charTree.end()) {
			s[i] = treeIter->first;
			isPossible = true;
			charFrequency[s[i]]--;
			// reorder the chars we've seen in ascending order.
			treeIter = charTree.begin();
			for (int j = i + 1; j < s.length(); ++j) {
				// find the next letter that has instances left to use
				while (charFrequency[treeIter->first] == 0)
					treeIter++;
				s[j] = treeIter->first;
				charFrequency[treeIter->first]--;
			}
			break;
		}
	}

	if (!isPossible)
		return "no answer";

	return s;

}

int main() {

	int numCases;
	cin >> numCases;
	vector<string> inputs(numCases);

	for (int i = 0; i < numCases; ++i) {
		cin >> inputs[i];
	}

	for (int i = 0; i < numCases; ++i) {
		cout << getBigger(inputs[i]) << "\n";
	}

	return 0;
}